import React, {Component} from 'react';

import Modal from '../components/Modal';

class Dash1 extends Component {
  state = {
    isLoading: false,
    passwords: [
      {
        target: "google.com",
        password: "asdqawerwf"
      }, {
        target: "yandex.com",
        password: "asdasdfsfsdadsa"
      }, {
        target: "yandex.com",
        password: "asdasdaeqwed"
      }
    ],
    showModal: false
  };

  handleTargetChange(event, key) {
    let oldState = this.state;
    oldState.passwords[key].target = event.target.value;
    this.setState(oldState);
  }

  handleShowModal = () => {
    this.setState({showModal: true});
  };

  handleCloseModal = () => {
    this.setState({showModal: false});
  };

  handlePasswordChange(event, key) {
    let oldState = this.state;
    debugger
    oldState.passwords[key].password = event.target.value;
    this.setState(oldState)
  }

  handleClickOnPassword(event, key, password) {
    event.target.value = password;
    this.handlePasswordChange(event, key);
    event.preventDefault();
  }

  render() {
    return (
      <div className="main-landing">
        <h1>Dashboard #1</h1>
        <p>Showing your password</p>
        <Modal
          show={this.state.showModal}
          closeModal={() => this.handleCloseModal()}
        />
        <div className="table-container">
          <table>
            <tr>
              <th>#</th>
              <th>target</th>
              <th>password</th>
              <th>Actions</th>
            </tr>
            <tbody>
            {this.state.passwords.map((passObject, i) => {
              return (
                <tr key={i + 1}>
                  <td>{i + 1}</td>
                  <td>
                    <input
                      value={passObject.target}
                      onChange={(event) =>  this.handleTargetChange(event, i)}
                      type="text"
                      placeholder="Target"
                    />
                  </td>
                  <td>
                    <input
                      value={"*"}
                      onChange={(event) =>  this.handlePasswordChange(event, i)}
                      onClick={(event) => this.handleClickOnPassword(event, i, passObject.password)}
                      type="text"
                      placeholder="Password"
                    />
                  </td>
                  <td>
                    <button
                      className="edit-button"
                      onClick={this.handleShowModal}
                    >Edit</button>
                    <button className="delete-button">Delete</button>
                  </td>
                </tr>
              )
            })}
            </tbody>
          </table>
        </div>
      </div>
    )
  }
};

export default Dash1;