import * as firebase from 'firebase';

const config = {
  apiKey: "AIzaSyB4E9e5HTjg4Fh5xdE9hk932TqoAa9J7UU",
  authDomain: "rreact-auth.firebaseapp.com",
  databaseURL: "https://rreact-auth.firebaseio.com",
  projectId: "rreact-auth",
  storageBucket: "rreact-auth.appspot.com",
  messagingSenderId: "185970544347"
};

if (!firebase.apps.length) {
  firebase.initializeApp(config);
};

const auth = firebase.auth();

export {
  auth,
};