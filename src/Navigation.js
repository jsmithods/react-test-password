import React from 'react';
import { Link } from 'react-router-dom';

import * as routes from './constants/routes';

const Navigation = () =>
  <div>
    <ul>
      <li><Link to={routes.SIGN_IN}>Login</Link></li>
      <li><Link to={routes.HOME}>Register</Link></li>
      <li><Link to={routes.LANDING}>Dashboard</Link></li>
      <li><Link to={routes.DASHNUM1}>Dash#1</Link></li>
      <li><Link to={routes.ACCOUNT}>Account</Link></li>
    </ul>
  </div>

export default Navigation;