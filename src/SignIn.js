import React from 'react';

const SignInPage = () => {
  return (
    <div className="login-card">
      <h1>Log-in</h1><br />
      <form>
        <input type="text" name="user" placeholder="Username" />
        <input type="password" name="pass" placeholder="Password" />
        <input type="submit" name="login" className="login login-submit" value="login" />
      </form>
      <div className="login-help">
        <a href="#">Register</a> • <a href="#">Forgot Password</a>
      </div>
    </div>
  )
};

export default SignInPage;